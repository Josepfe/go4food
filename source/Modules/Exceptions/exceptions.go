package exceptions

type showErrorMessage interface {
	Show() string
}

type Exception struct {
	StatusCode             int32
	ShowCustomErrorMessage bool
}
