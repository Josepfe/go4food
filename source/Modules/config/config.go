package config

import (
	"fmt"

	"github.com/spf13/viper"
)

// Config is global object that holds all application level variables.
var Config appConfig

type appConfig struct {
	// the server port. Defaults to 8080
	ServerPort int `mapstructure:"server_port"`
	// the API key needed to authorize to API. required.
	ApiKey string `mapstructure:"api_key"`
	// rabbitMq
	RabbitMqConfig rabbitMqConfig `mapstructure:"rabbitMq"`
}

type rabbitMqConfig struct {
	Host             string `mapstructure:"host"`
	Port             int    `mapstructure:"port"`
	UserName         string `mapstructure:"username"`
	Password         string `mapstructure:"password"`
	DefaultQueueName string `mapstructure:"defaultQueueName"`
}

// LoadConfig loads config from files
func LoadConfig(configPaths ...string) error {

	viper := viper.New()
	viper.AddConfigPath(configPaths[len(configPaths)-1])
	viper.SetConfigName("development")
	viper.SetConfigType("yaml")
	viper.SetDefault("ContentDir", "content")

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %w \n", err))
	}

	return viper.Unmarshal(&Config)
}
