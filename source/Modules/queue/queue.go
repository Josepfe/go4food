package queue

import (
	"fmt"
	"log"

	config "go4food/Modules/config"

	"github.com/streadway/amqp"
)

func PublishMessage(message string, queueName_optional ...string) {

	conn, queue, ch := configQueue(queueName_optional)
	defer conn.Close()
	defer ch.Close()

	//publish message
	err := ch.Publish(
		"",         // exchange
		queue.Name, // routing key
		false,      // mandatory
		false,      // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(message),
		})
	failOnError(err, "Failed to publish a message")
}

func QueueReceiver(queueName_optional ...string) {
	conn, queue, ch := configQueue(queueName_optional)
	defer conn.Close()
	defer ch.Close()

	msgs, err := ch.Consume(
		queue.Name, // queue
		"",         // consumer
		true,       // auto-ack
		false,      // exclusive
		false,      // no-local
		false,      // no-wait
		nil,        // args
	)
	failOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			log.Printf("Received a message: %s", d.Body)
			//clasificar los mensajes y mandarlos al handler correspondiente
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func configQueue(queueName_optional []string) (*amqp.Connection, amqp.Queue, *amqp.Channel) {
	queueConection := fmt.Sprintf("amqp://%s:%s@%s:%d/",
		config.Config.RabbitMqConfig.UserName,
		config.Config.RabbitMqConfig.Password,
		config.Config.RabbitMqConfig.Host,
		config.Config.RabbitMqConfig.Port)

	//get queue connection
	conn, err := amqp.Dial(queueConection)
	failOnError(err, "Failed to connect to RabbitMQ")

	//get channel
	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")

	//get queue name
	queueName := config.Config.RabbitMqConfig.DefaultQueueName
	if len(queueName_optional) > 0 {
		queueName = queueName_optional[0]
	}

	// declare queue
	q, err := ch.QueueDeclare(
		queueName, // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	failOnError(err, "Failed to declare a queue")

	return conn, q, ch
}
