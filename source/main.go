package main

import (
	"fmt"
	config "go4food/Modules/config"
	server "go4food/Modules/httpServer"
	queue "go4food/Modules/queue"
)

// @title Go4Food Swagger API
// @version 1.0
// @description Swagger API for Golang Project Go4Food.
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.email martin7.heinz@gmail.com

// @license.name MIT
// @license.url https://github.com/MartinHeinz/go-project-blueprint/blob/master/LICENSE

// @BasePath /api/v1

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {

	if err := config.LoadConfig("./config"); err != nil {
		panic(fmt.Errorf("invalid application configuration: %s", err))
	}

	queue.QueueReceiver()
	server.StartServer()
}
