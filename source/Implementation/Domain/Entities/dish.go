package entities_dish

type Dish struct {
	ID     string  `json:"id"`
    Name  string  `json:"name"`
    Price float64  `json:"price"`
    Description  string `json:"description"`
}

