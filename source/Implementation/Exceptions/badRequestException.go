package impl_exceptions

import (
	iExceptions "go4food/Modules/Exceptions"
)

type BadRequestException struct {
	iExceptions.Exception
}

func (ex BadRequestException) Show(show bool) string {
	ex.StatusCode = 400
	ex.ShowCustomErrorMessage = show

	if show {
		return "My message bro"
	}
	return ""
}
