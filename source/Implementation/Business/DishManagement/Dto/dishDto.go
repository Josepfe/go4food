package dto_dish

import (
	dish "go4food/Implementation/Domain/Entities"
)

type DishDto struct {
	Name        string  `json:"name"`
	Price       float64 `json:"price"`
	Description string  `json:"description"`
}

func EntityToDto(d dish.Dish) DishDto {
	return DishDto{Name: d.Name, Price: d.Price, Description: d.Description}
}
