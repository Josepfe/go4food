package controller_dishes

import (
	// Ttry to refactor this ugly import
	dishService "go4food/Implementation/Business/DishManagement/Service"
	"net/http"

	"github.com/gin-gonic/gin"
)

func RegisterDishManagementHandlers(r *gin.RouterGroup) {
	r.GET("/dishes", getAllDishesHandler)
	r.GET("/bravas", getPatatasBravasHandler)
	r.GET("/ham", getJamonHandler)
	r.GET("/salad", getEnsaladillaHandler)
}

/****************************************
*										*
*				HANDLERS		*
*										*
****************************************/
// Get All Dishes godoc
// @Tags DishesController
// @Summary Retrieves all dishes
// @Produce json
// @Success 200 {object} dto_dish.DishDto
// @Failure 401
// @Router /dish-management/dishes [GET]
// @Security ApiKeyAuth
func getAllDishesHandler(c *gin.Context) {
	//Todo create service
	dishes := <-dishService.GetAllDishes()
	c.IndentedJSON(http.StatusOK, dishes)
}

// Get Patatas Bravas godoc
// @Tags DishesController
// @Summary Retrieves Patatas Bravas
// @Produce json
// @Success 200 {object} dto_dish.DishDto
// @Failure 401
// @Router /dish-management/bravas [GET]
// @Security ApiKeyAuth
func getPatatasBravasHandler(c *gin.Context) {
	//Todo create service
	bravas := <-dishService.GetPatatasBravas()
	c.IndentedJSON(http.StatusOK, bravas)
}

// Get Jamon godoc
// @Tags DishesController
// @Summary Retrieves Jamon
// @Produce json
// @Success 200 {object} dto_dish.DishDto
// @Failure 401
// @Router /dish-management/ham [GET]
// @Security ApiKeyAuth
func getJamonHandler(c *gin.Context) {
	//Todo create service
	ham := <-dishService.GetJamon()
	c.IndentedJSON(http.StatusOK, ham)
}

// Get Ensaladilla godoc
// @Tags DishesController
// @Summary Retrieves Ensaladilla
// @Produce json
// @Success 200 {object} dto_dish.DishDto
// @Failure 401
// @Router /dish-management/salad [GET]
// @Security ApiKeyAuth
func getEnsaladillaHandler(c *gin.Context) {
	//Todo create service
	ensaladilla := <-dishService.GetEnsaladilla()
	c.IndentedJSON(http.StatusOK, ensaladilla)
}
