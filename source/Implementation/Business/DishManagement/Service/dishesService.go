package services_dishes

import (
	dishDto "go4food/Implementation/Business/DishManagement/Dto"
	dish "go4food/Implementation/Domain/Entities"
)

func GetPatatasBravas() <-chan dishDto.DishDto {

	c := make(chan dishDto.DishDto)
	go func() {
		dish := dish.Dish{
			ID:          "1",
			Name:        "Patatas Bravas",
			Price:       5,
			Description: "The traditional Patatas Bravas from Spain, spicy with the special home-made 'alioli' sauce",
		}
		c <- dishDto.EntityToDto(dish)
	}()

	return c
}

func GetJamon() <-chan dishDto.DishDto {

	c := make(chan dishDto.DishDto)
	go func() {
		dish := dish.Dish{
			ID:          "2",
			Name:        "Jamon",
			Price:       4,
			Description: "Tipical spanish jamon",
		}
		c <- dishDto.EntityToDto(dish)
	}()
	return c
}

func GetEnsaladilla() <-chan dishDto.DishDto {

	c := make(chan dishDto.DishDto)
	go func() {
		dish := dish.Dish{
			ID:          "3",
			Name:        "Ensaladilla Rusa",
			Price:       4,
			Description: "Salad with maionaise, olives, potatoe, pickles, tuna...",
		}
		c <- dishDto.EntityToDto(dish)
	}()

	return c
}

func GetAllDishes() <-chan []dishDto.DishDto {

	c := make(chan []dishDto.DishDto)
	go func() {
		dishes := []dishDto.DishDto{}
		dishes = append(dishes, <-GetPatatasBravas())
		dishes = append(dishes, <-GetJamon())
		dishes = append(dishes, <-GetEnsaladilla())
		c <- dishes
	}()
	return c
}
