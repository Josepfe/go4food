package services_dishes_test

import (
	dishService "go4food/Implementation/Business/DishManagement/Service"
	"testing"
)

func TestPatatasBravas(t *testing.T) {
	ans := <-dishService.GetPatatasBravas()
	if ans.Name != "Patatas Bravas" {
		t.Errorf("The name %s is not Patatas Bravas", ans.Name)
	}
	if ans.Price != 5 {
		t.Errorf("The price %f is not 5", ans.Price)
	}
	if ans.Description != "The traditional Patatas Bravas from Spain, spicy with the special home-made 'alioli' sauce" {
		t.Errorf("The description %s is not the expected", ans.Description)
	}
}

func TestJamon(t *testing.T) {
	ans := <-dishService.GetJamon()
	if ans.Name != "Jamon" {
		t.Errorf("The name %s is not Jamon", ans.Name)
	}
	if ans.Price != 4 {
		t.Errorf("The price %f is not 4", ans.Price)
	}
	if ans.Description != "Tipical spanish jamon" {
		t.Errorf("The description %s is not the expected", ans.Description)
	}
}

func TestEnsaladilla(t *testing.T) {
	ans := <-dishService.GetEnsaladilla()
	if ans.Name != "Ensaladilla Rusa" {
		t.Errorf("The name %s is not Ensaladilla Rusa", ans.Name)
	}
	if ans.Price != 4 {
		t.Errorf("The price %f is not 4", ans.Price)
	}
	if ans.Description != "Salad with maionaise, olives, potatoe, pickles, tuna..." {
		t.Errorf("The description %s is not the expected", ans.Description)
	}
}

func TestGetAllDishes(t *testing.T) {
	ans := <-dishService.GetAllDishes()
	if len(ans) != 3 {
		t.Errorf("The size of the array is not the desired")
	}
}
