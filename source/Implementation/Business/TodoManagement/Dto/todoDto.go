package todo_dto

type Todo struct {
	Id          uint32 `json:"id"`
	Description string `json:"description"`
	Done        bool   `json:"done"`
}
