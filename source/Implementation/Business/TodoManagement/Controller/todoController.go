package todo_controller

import (
	// Ttry to refactor this ugly import
	todo "go4food/Implementation/Business/TodoManagement/Dto"
	"net/http"

	"github.com/gin-gonic/gin"
)

var router *gin.Engine = gin.Default()

func GetAllTodos(c *gin.Context) {
	router.GET("/todos", fakeTodo)
}

func fakeTodo(c *gin.Context) {
	//Todo create service
	var myTodo = todo.Todo{Description: "Josep smeller",
		Done: true}
	c.IndentedJSON(http.StatusOK, myTodo)
}
